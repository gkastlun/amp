.. Amp documentation master file, created by
   sphinx-quickstart on Thu Jul 30 17:27:50 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Amp: Atomistic Machine-learning Package
=======================================

`Amp <https://bitbucket.org/andrewpeterson/amp>`_ is an open-source package designed to easily bring machine-learning to atomistic calculations.
This project is being developed at Brown University in the School of Engineering, primarily by **Andrew Peterson** and **Alireza Khorshidi**,
and is released under the GNU General Public License.

The latest stable release of Amp is version 0.5, released on February 24, 2017; see the :ref:`ReleaseNotes` page for a download link.
Please see the project's `git repository <https://bitbucket.org/andrewpeterson/amp>`_ for the latest development version or a place to report an issue.

You can read about Amp in the below paper; if you find Amp useful, we would appreciate if you cite this work:

    Khorshidi & Peterson, "Amp: A modular approach to machine learning in atomistic simulations", *Computer Physics Communications* 207:310-324, 2016. |amp_paper|


.. |amp_paper| raw:: html

   <a href="http://dx.doi.org/10.1016/j.cpc.2016.05.010" target="_blank">DOI:10.1016/j.cpc.2016.05.010</a>

**Manual**:

.. toctree::
   :maxdepth: 1

   introduction.rst
   theory.rst
   credits.rst
   releasenotes.rst
   installation.rst
   useamp.rst
   examplescripts.rst
   analysis.rst
   neuralnetwork.rst
   building.rst
   moredescriptor.rst
   moremodel.rst
   tensorflow.rst
   databases.rst
   develop.rst

**Module autodocumentation**:

.. toctree::
   :maxdepth: 1

   modules/main.rst
   modules/descriptor.rst
   modules/model.rst
   modules/regression.rst
   modules/utilities.rst
   modules/analysis.rst


**Indices and tables**

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

